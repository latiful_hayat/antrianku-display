package com.stufimedia.antrianku.antriankuclient36a;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.view.WindowManager;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.TextView;
import android.widget.Toast;

import com.mikhaellopez.circularimageview.CircularImageView;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;


public class MainActivity extends AppCompatActivity {
    public static final String EXTRA_MESSAGE = "com.stufimedia.antrianku.antriankuclient36a.MESSAGE";

    TextView textviewNomor, textviewLoket;
    TextView nama;
    private static final int DEFAULT_PORT = 8086;

    //SIMPLEHTTPSERVER
    ServerSocket httpServerSocket;
    String msgLog = ""; String loket = ""; String nomor = "";
    boolean startted = false;

    SharedPreferences pref;
    String stringLog = "";

    boolean permisionGranted = false;
    private TextView namaLayanan;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Check permission
        permisionGranted = isReadStoragePermissionGranted() && isNetworkPermissionGranted() && isInternetPermissionGranted();
        if(permisionGranted){
            //new MyCountDownTimer(5000, 1000).start();
            stringLog += "Permission: Granted\n";
        }else{
            stringLog += "Permission: Rejected\n";
        }

        //GetScreenSize
        DisplayMetrics displaymetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
        int height = displaymetrics.heightPixels;
        int width = displaymetrics.widthPixels;

        //Read Preferences
        pref = getApplicationContext().getSharedPreferences("AntriankuClient", 0); // 0 - for private mode

        //Read Background filename
        String BGFile = pref.getString("BG","");
        if (BGFile!="") {
            LoadBackground(BGFile);
        }else{
            Toast.makeText(MainActivity.this, "Ukuran Backgroud: " + width + " x " + height, Toast.LENGTH_LONG).show();
        }

        //Prevent sleep
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        //Set to landscape
        this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);

        //Enable toolbar
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);



        setTitle("AntrianKu Display");

        textviewNomor = findViewById(R.id.textview);
        textviewNomor.setText("Sentuh menu dan pilih aktifkan, atau pilih setting");

        namaLayanan = findViewById(R.id.textView2);
        namaLayanan.setText("");

        nama = findViewById(R.id.textViewNama);
        nama.setText(pref.getString("PERSONNAME",""));

        //setFullScreen();
        //startTheServer();
    }

    @Override
    protected void onStart() {
        super.onStart();
        if(!startted)
        {
//            startTheServer();
//            startted = true;
        }
    }
    private void setIpAccess() {
        textviewNomor.setText(getIpAccess());
    }
    private String getIpAccess() {
        WifiManager wifiManager = (WifiManager) getBaseContext().getApplicationContext().getSystemService(WIFI_SERVICE);
        int ipAddress = wifiManager.getConnectionInfo().getIpAddress();
        final String formatedIpAddress = String.format("%d.%d.%d.%d", (ipAddress & 0xff), (ipAddress >> 8 & 0xff), (ipAddress >> 16 & 0xff), (ipAddress >> 24 & 0xff));
        return "http://" + formatedIpAddress + ":";
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        // ignore orientation/keyboard change, always in landscape
        // add in manifest --> android:screenOrientation="landscape" android:configChanges="orientation|keyboardHidden"
        super.onConfigurationChanged(newConfig);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        if (httpServerSocket != null) {
            try {
                httpServerSocket.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_fullscreen) {
            //setFullScreen();
            if(!startted){
                startTheServer();
                startted = true;
                item.setVisible(false);//.setEnabled(false);
                hideStatusBar();
                getSupportActionBar().hide();
                hideToolBr();
                //findViewById(R.id.action_removeui).setVisibility(View.VISIBLE);
                //findViewById(R.id.action_fullscreen).setVisibility(View.INVISIBLE);
            }
            return true;
        }
        if (id == R.id.action_removeui) {
            setFullScreen();
            hideToolBr();
            return true;
        }
        if (id == R.id.action_info){
            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
            alertDialogBuilder.setMessage(getIpAddress() + ":" + DEFAULT_PORT);
            AlertDialog alertDialog = alertDialogBuilder.create();
            alertDialog.show();
        }
        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            Intent intent = new Intent(this, Setting.class);
            intent.putExtra(EXTRA_MESSAGE, "Setting");
            startActivity(intent);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
    private Bitmap ImgBitFromFile(String file_name) {
        File imgFile = new File(file_name);
        Bitmap bmp = null;
        if (imgFile.exists()) {
            stringLog += "File Exist: "+ imgFile.getAbsolutePath()+"\n";
            File f = new File(imgFile.getAbsolutePath());
            BitmapFactory.Options options = new BitmapFactory.Options();
            //options.inPreferredConfig = Bitmap.Config.ARGB_8888;
            try {
                bmp = BitmapFactory.decodeStream(new FileInputStream(f), null, options);
            } catch (Exception e) {
                stringLog += "decodeStream Error: "+ e.getMessage()+"\n";
                e.printStackTrace();
            }
            if(bmp==null){
                stringLog += "BitmapFactory: null"+"\n";
            }
        }
        return bmp;
    }
    private void startTheServer() {
        try {
//            textviewNomor.setText(getIpAddress() + ":"
//                    + HttpServerThread.HttpServerPORT + "\n");
            HttpServerThread httpServerThread = new HttpServerThread();
            httpServerThread.start();
            textviewNomor.setTextSize(TypedValue.COMPLEX_UNIT_SP,300);
            textviewNomor.setTextColor(Color.rgb(0,0,255));
            textviewNomor.setText("----");
            setTitle("Menunggu panggilan");
        } catch (Exception e) {
            e.printStackTrace();
            textviewNomor.setText("Server failed: " + e.toString());
        }
    }
    public  boolean isReadStoragePermissionGranted() {
        if (Build.VERSION.SDK_INT >= 23) {
            if (checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE)
                    == PackageManager.PERMISSION_GRANTED) {
                return true;
            } else {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 3);
                return false;
            }
        }
        else { //permission is automatically granted on sdk<23 upon installation
            return true;
        }
    }
    public  boolean isNetworkPermissionGranted() {
        if (Build.VERSION.SDK_INT >= 23) {
            if (checkSelfPermission(Manifest.permission.ACCESS_NETWORK_STATE)
                    == PackageManager.PERMISSION_GRANTED) {
                return true;
            } else {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_NETWORK_STATE}, 2);
                return false;
            }
        }
        else { //permission is automatically granted on sdk<23 upon installation
            return true;
        }
    }
    public  boolean isInternetPermissionGranted() {
        if (Build.VERSION.SDK_INT >= 23) {
            if (checkSelfPermission(Manifest.permission.INTERNET)
                    == PackageManager.PERMISSION_GRANTED) {
                return true;
            } else {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.INTERNET}, 1);
                return false;
            }
        }
        else { //permission is automatically granted on sdk<23 upon installation
            return true;
        }
    }
    private  void hideStatusBar() {
        if (Build.VERSION.SDK_INT < 16) {
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                    WindowManager.LayoutParams.FLAG_FULLSCREEN);
        }else {
            View decorView = getWindow().getDecorView();    // Hide the status bar.
            int uiOptions = View.SYSTEM_UI_FLAG_FULLSCREEN;
            decorView.setSystemUiVisibility(uiOptions);
        }

    }
    private void setFullScreen() {
        hideStatusBar();
        // Remember that you should never show the action bar if the
        getSupportActionBar().hide();
    }
    //PRIVATE CLASS
    String getIpAddress()
    {
        return  getIpAccess();
    }
    private class HttpServerThread extends Thread {

        static final int HttpServerPORT = DEFAULT_PORT;

        @Override
        public void run() {
            Socket socket = null;

            try {
                httpServerSocket = new ServerSocket(HttpServerPORT);

                while(true){
                    socket = httpServerSocket.accept();

                    HttpResponseThread httpResponseThread =
                            new HttpResponseThread(
                                    socket,
                                    "OK");
                    httpResponseThread.start();
                }
            } catch (IOException e) {
                textviewNomor.setText("httpServerSocket: " + e.getMessage());
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

        }


    }
    private class HttpResponseThread extends Thread {

        Socket socket;
        String h1;

        HttpResponseThread(Socket socket, String msg){
            this.socket = socket;
            h1 = msg;
        }

        @Override
        public void run() {
            BufferedReader is;
            PrintWriter os;
            String request;


            try {
                is = new BufferedReader(new InputStreamReader(socket.getInputStream()));
                request = is.readLine();

                os = new PrintWriter(socket.getOutputStream(), true);

                String response =
                        "<html><head></head>" +
                                "<body>" + h1 +
                                "</body></html>";

                os.print("HTTP/1.0 200" + "\r\n");
                os.print("Content type: text/html" + "\r\n");
                os.print("Content length: " + response.length() + "\r\n");
                os.print("\r\n");
                os.print(response + "\r\n");
                os.flush();
                socket.close();


//                msgLog += "Request of " + request
//                        + " from " + socket.getInetAddress().toString() + "\n";
                msgLog = "" + request;
                if(request.contains("GET /")){
                    try {
                        int httpLoc = request.indexOf("HTTP");
                        int equalLoc = request.indexOf("?");
                        if (httpLoc > 1 && equalLoc > 1) {
                            msgLog = request.substring(equalLoc, httpLoc - 1);
                            int andLoc = msgLog.indexOf("&");
                            if (andLoc > 0) {
                                loket = msgLog.substring(0, andLoc);
                                loket = loket.replace("%20", " ");
//                                loket = loket.replace("NAMA=", "");
                                equalLoc = loket.indexOf("=");
                                if (equalLoc > 0) {
                                    loket = loket.substring(equalLoc + 1);
                                }
                                nomor = msgLog.substring(andLoc);
                                equalLoc = nomor.indexOf("=");
                                if (equalLoc > 0) {
                                    nomor = nomor.substring(equalLoc + 1);
                                }
                            }
                        }
                        msgLog = loket + nomor;
                    }catch (Exception e)
                    {
                        textviewNomor.setText("Parsing: " + e.getMessage());
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                }
                else {

                    msgLog = "";
                }
                MainActivity.this.runOnUiThread(new Runnable() {

                    @Override
                    public void run() {
                        if(!msgLog.equals("")){
                            textviewNomor.setText(nomor);
//                            Animation anim = new AlphaAnimation(0.0f, 1.0f);
//                            anim.setDuration(50); //You can manage the blinking time with this parameter
//                            anim.setStartOffset(20);
//                            anim.setRepeatMode(Animation.REVERSE);
//                            anim.setRepeatCount(5);
//                            textviewNomor.startAnimation(anim);
                        }
                        setTitle(loket);
                        namaLayanan.setText(loket);
                    }
                });

            } catch (IOException e) {
                textviewNomor.setText("Thread: " + e.getMessage());
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            return;
        }
    }
    @Override
    protected void onResume() {
        super.onResume();
        //after Paused (another activity come into foreground)
        this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE); //Set to landscape

        DisplayMetrics displaymetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
        int height = displaymetrics.heightPixels;
        int width = displaymetrics.widthPixels;

        String BGFile = pref.getString("BG","");
        if (BGFile!="") {
            LoadBackground(BGFile);
        }else{
            Toast.makeText(MainActivity.this, "Ukuran Backgroud: " + width + " x " + height, Toast.LENGTH_LONG).show();
        }

        nama.setText(pref.getString("PERSONNAME", ""));

        if(permisionGranted)
        {
            RefreshScreen();
        }
        getSupportActionBar().show();
    }
    private void LoadBackground(String bgFile) {
        //Background
        ConstraintLayout mConstraintLayout =  findViewById(R.id.mainLayout);
        String bgFiles = pref.getString("BG", "");
        if (bgFiles != "") {
            try {
                Drawable drw = null;
                drw = Drawable.createFromPath(bgFiles);
                if(drw==null){
                    stringLog += "Err: File tidak terbaca"+"\n";
                }else {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                        mConstraintLayout.setBackground(drw);
                    }else{
                        Toast.makeText(MainActivity.this, "Versi Android harus Jelly Bean atau yang lebih baru", Toast.LENGTH_LONG).show();
                    }
                }
            }catch (Exception e){
                stringLog += "Set Backgroud: Error " + e.getMessage()+"\n";
            }
        }else {
            stringLog += "File BG tidak tersedia " + "\n";
        }
    }
    private void RefreshScreen(){
    }
    @Nullable
    private Bitmap getBitmapFromFile(String imgbtn1bg) {
        Bitmap b = null;
        if(imgbtn1bg!="") {
            try {
                b = ImgBitFromFile(imgbtn1bg);
            } catch (Exception e) {
                stringLog += "ImgBitFromFile Err: " +e.getMessage()+"\n";
            }
        }
        return b;
    }
    public void hideToolBr(){

        // BEGIN_INCLUDE (get_current_ui_flags)
        // The UI options currently enabled are represented by a bitfield.
        // getSystemUiVisibility() gives us that bitfield.
        int uiOptions = getWindow().getDecorView().getSystemUiVisibility();
        int newUiOptions = uiOptions;
        // END_INCLUDE (get_current_ui_flags)
        // BEGIN_INCLUDE (toggle_ui_flags)
        boolean isImmersiveModeEnabled =
                ((uiOptions | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY) == uiOptions);
        if (isImmersiveModeEnabled) {
            //Log.i(Constants.TAG_DEF, "Turning immersive mode mode off. ");
        } else {
            //Log.i(Constants.TAG_DEF, "Turning immersive mode mode on.");
        }

        // Navigation bar hiding:  Backwards compatible to ICS.
        if (Build.VERSION.SDK_INT >= 14) {
            newUiOptions ^= View.SYSTEM_UI_FLAG_HIDE_NAVIGATION;
        }

        // Status bar hiding: Backwards compatible to Jellybean
        if (Build.VERSION.SDK_INT >= 16) {
            newUiOptions ^= View.SYSTEM_UI_FLAG_FULLSCREEN;
        }

        // Immersive mode: Backward compatible to KitKat.
        // Note that this flag doesn't do anything by itself, it only augments the behavior
        // of HIDE_NAVIGATION and FLAG_FULLSCREEN.  For the purposes of this sample
        // all three flags are being toggled together.
        // Note that there are two immersive mode UI flags, one of which is referred to as "sticky".
        // Sticky immersive mode differs in that it makes the navigation and status bars
        // semi-transparent, and the UI flag does not get cleared when the user interacts with
        // the screen.
        if (Build.VERSION.SDK_INT >= 18) {
            newUiOptions ^= View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY;
        }

        getWindow().getDecorView().setSystemUiVisibility(newUiOptions);
        //END_INCLUDE (set_ui_flags)

    }
}

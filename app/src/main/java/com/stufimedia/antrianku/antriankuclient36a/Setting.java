package com.stufimedia.antrianku.antriankuclient36a;

import android.Manifest;
import android.content.Context;
import android.content.CursorLoader;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class Setting extends AppCompatActivity {
    private SharedPreferences pref;
    EditText editText,BGFiles,PhotoFiles;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.setting);
        Intent intent = getIntent();
        String message = intent.getStringExtra(MainActivity.EXTRA_MESSAGE);

        //Read from pref
        pref = getApplicationContext().getSharedPreferences("AntriankuClient", 0); // 0 - for private mode

        editText = findViewById(R.id.editTextTextPersonName);
        editText.setText(pref.getString("PERSONNAME", ""));
        BGFiles = findViewById(R.id.editTextBG);
        BGFiles.setText(pref.getString("BG", ""));
        PhotoFiles = findViewById(R.id.editTextPhoto);
        PhotoFiles.setText(pref.getString("PHOTO", ""));
    }

    public void OnGambarClick(View view) {
        //Ganti gambar
        OpenImageFile(1);
    }

    private void OpenImageFile(int actionRequestCode) {
        if (Build.VERSION.SDK_INT < 19) {
            Intent intent = new Intent();
            intent.setType("image/*");
            intent.setAction(Intent.ACTION_GET_CONTENT);
            startActivityForResult(
                    Intent.createChooser(intent, "Select Picture"),
                    actionRequestCode);

        } else {
            Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
            intent.addCategory(Intent.CATEGORY_OPENABLE);
            intent.setType("image/*");
            startActivityForResult(intent, actionRequestCode);
        }
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode != RESULT_OK) return;
        if(!isReadStoragePermissionGranted())
        {
            Toast.makeText(Setting.this, "Tidak diizinkan membuka file", Toast.LENGTH_LONG).show();
            return;
        }
        String path     = "";
        if(requestCode == 1) //Background
        {
            Uri uri = data.getData();
            String filePath = "";
            filePath = getFilePathNew(getBaseContext(),uri);
            Toast.makeText(Setting.this, "File terpilih:" +filePath, Toast.LENGTH_LONG).show();
            //Save to pref
            BGFiles.setText(filePath);
        }
        if(requestCode == 2) //foto
        {
            Uri uri = data.getData();
            String FilePath = getFilePathNew(this.getBaseContext(), uri); // should the path be here in this string
            Toast.makeText(Setting.this, "File terpilih:" +FilePath, Toast.LENGTH_LONG).show();
            //Save to pref
//            SharedPreferences.Editor editor = pref.edit();
//            editor.putString("BG1", FilePath); // Storing string
//            editor.commit();
            PhotoFiles.setText(FilePath);
        }
    }

    String getFilePath(Context cntx, Uri uri) {
        Cursor cursor = null;
        try {
            String[] arr = { MediaStore.Images.Media.DATA };
            cursor = cntx.getContentResolver().query(uri,  arr, null, null, null);
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            return cursor.getString(column_index);
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        SharedPreferences.Editor editor = pref.edit();
        editor.putString("PERSONNAME", editText.getText().toString());

        editor.putString("BG", String.valueOf(BGFiles.getText())); // Storing string
        editor.putString("PHOTO", String.valueOf(PhotoFiles.getText())); // Storing string
        editor.commit();

    }
    public static String getFilePathNew(Context context, Uri uri)
    {
        int currentApiVersion;
        try
        {
            currentApiVersion = android.os.Build.VERSION.SDK_INT;
        }
        catch(NumberFormatException e)
        {
            //API 3 will crash if SDK_INT is called
            currentApiVersion = 3;
        }
        if (currentApiVersion >= Build.VERSION_CODES.KITKAT)//>19
        {
            String filePath = null;
            String wholeID = null;
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.KITKAT) {
                wholeID = DocumentsContract.getDocumentId(uri);
            }

            // Split at colon, use second item in the array
            String id = wholeID.split(":")[1];

            String[] column = {MediaStore.Images.Media.DATA};

            // where id is equal to
            String sel = MediaStore.Images.Media._ID + "=?";

            Cursor cursor = context.getContentResolver().query(MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                    column, sel, new String[]{id}, null);
            if(cursor!=null) {
                int columnIndex = cursor.getColumnIndex(column[0]);

                if (cursor.moveToFirst()) {
                    filePath = cursor.getString(columnIndex);
                }
                cursor.close();
            }
            return filePath;
        }
        else if (currentApiVersion <= Build.VERSION_CODES.HONEYCOMB_MR2 && currentApiVersion >= Build.VERSION_CODES.HONEYCOMB)//11..13

        {
            String[] proj = {MediaStore.Images.Media.DATA};
            String result = null;

            CursorLoader cursorLoader = new CursorLoader(
                    context,
                    uri, proj, null, null, null);
            Cursor cursor = cursorLoader.loadInBackground();

            if (cursor != null)
            {
                int column_index =
                        cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
                cursor.moveToFirst();
                result = cursor.getString(column_index);
            }
            return result;
        }
        else//14..18
        {
            String result = null;
            String[] proj = {MediaStore.Images.Media.DATA};
            Cursor cursor = context.getContentResolver().query(uri, proj, null, null, null);
            if (cursor!=null) {
                int column_index
                        = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
                cursor.moveToFirst();
                result = cursor.getString(column_index);
            }
            return result;
        }
    }

    public  boolean isReadStoragePermissionGranted() {
        if (Build.VERSION.SDK_INT >= 23) {
            if (checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE)
                    == PackageManager.PERMISSION_GRANTED) {
                return true;
            } else {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 3);
                return false;
            }
        }
        else { //permission is automatically granted on sdk<23 upon installation
            return true;
        }
    }

    public void OnPhotoClick(View view) {
        //Ganti gambar
        OpenImageFile(2);
    }
}
